<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <h3 class="mb-3 text-center">Insert your data</h3>
        <!-- Form start  -->
   <form action="/store" method="POST">
   @csrf
  <div class="mb-3">
    <label class="form-label">Post Title </label>
    <input type="text" name="title" class="form-control" >
   </div>
   
  <div class="mb-3">
    <label class="form-label">Post Author</label>
    <input type="text" name="author" class="form-control" >
  </div>
  
  <button type="submit" name="insert" class="btn btn-primary">Insert</button>

  </form>

    </div>
</body>
</html>