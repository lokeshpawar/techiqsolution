<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
 
 <div class="container mt-5">
  
 <table class="table table-bordered shadow text-center text-striped">
 <tr>
 <th>Post ID</th>
 <th>Post Title</th>
 <th>Post Author</th>
 <th>Post Delete</th>
 <th>Post Edit</th>


</tr>

@foreach($posts as $post)
<tr>
   <td>{{$post->id}}</td>
   <td>{{$post->post_title}}</td>
   <td>{{$post->post_author}}</td>
   <td><a href="/delete/{{$post->id}}" class="btn btn-danger">Delete</a></td>
   <td><a href="/edit/{{$post->id}}" class="btn btn-success">Edit</a></td>


</tr>
@endforeach


 </table>

 </div>
 

</body>
</html>